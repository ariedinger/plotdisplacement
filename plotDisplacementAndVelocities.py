#!/usr/bin/env python3

### ----------------------------------------
### LIBRARIES
### ----------------------------------------
import sys
import matplotlib.pyplot as plt
import numpy as np

### ----------------------------------------

### ----------------------------------------
### EXTRACT DATA
### ----------------------------------------
# Open file and store its contents line per line in a variable
with open(sys.argv[1]) as f:
    lines = f.readlines()

# Create empty non-numpy arrays to store the numbers
displacements = []
velocities = []
times = []

# The format in CCX's .dat file is atrocious, horrendous and the worst thing they could have done
# We need to get the numbers from there and store them in the arrays
for i, line in enumerate(lines):
    # If we find a line with the word displacements in it
    if "displacements" in line:
        # We search for the word "time" in that line, and the two next ones (the velocities ones)
        # Once we found it, we split (get) the last word in that line, which coincidentaly is the time number
        # Then we append it to the times array
        time = [line for line in lines[i:i+2] if "time" in line][0].split()[-1]
        time = float(time)
        times.append(time)

        # Since the line next to "displacements" is null, we have to search in the [i+2] line
        # There, we avoid the first integer (node number) and split (get) the other numbers
        displacements.append([float(x) for x in lines[i+2].split()[1:]])

    # Then we do the same if we find the word velocities
    elif "velocities" in line:
        velocities.append([float(x) for x in lines[i+2].split()[1:]])

# We create numpy matrix for the numbers found
t = np.array(times)
U = np.array(displacements)
V = np.array(velocities)

# And pass the numbers to arrays with the coordinates
Ux = U[:,0]
Uz = U[:,2]
Uy = U[:,1]
Vx = V[:,0]
Vz = V[:,2]
Vy = V[:,1]

### ----------------------------------------

### ----------------------------------------
### PLOT
### ----------------------------------------
# Plot Displacement in the X direction
plt.subplot(2,3,1)
plt.plot(t,Ux)
plt.title(r"$U_{x}$ Displacement")
plt.grid(True)

# Plot Displacement in the Y direction
plt.subplot(2,3,2)
plt.plot(t,Uy)
plt.title(r"$U_{y}$ Displacement")
plt.grid(True)

# Plot Displacement in the Z direction
plt.subplot(2,3,3)
plt.plot(t,Uz)
plt.title(r"$U_{z}$ Displacement")
plt.grid(True)

# Plot Displacement in the X direction
plt.subplot(2,3,4)
plt.plot(t,Vx)
plt.title(r"$V_{x}$ Velocity")
plt.grid(True)

# Plot Displacement in the Y direction
plt.subplot(2,3,5)
plt.plot(t,Vy)
plt.title(r"$V_{y}$ Velocity")
plt.grid(True)

# Plot Displacement in the z direction
plt.subplot(2,3,6)
plt.plot(t,Vy)
plt.title(r"$V_{z}$ Velocity")
plt.grid(True)

# Render plot
plt.suptitle(r"DISPLACEMENT AND VELOCITIES PLOT")
plt.show()

### ----------------------------------------
